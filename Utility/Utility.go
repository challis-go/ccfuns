package utility

import (
	"io"
	"os"
)

func IsExist(path string) (bool, error) {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true, nil
		}
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func CopyFile(src string, dest string, override bool) error {
	fin, err := os.Open(src)
	if err != nil {
		return err
	}
	defer fin.Close()
	if override {
		exist, err := IsExist(dest)
		if err != nil {
			return err
		}
		if exist {
			os.Remove(dest)
		}
	}
	fout, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer fout.Close()
	_, err = io.Copy(fout, fin)
	return err
}

func DateToYMD(date int) (int, int, int) {
	year := date / 10000
	month := (date % 10000) / 100
	day := date % 100
	return year, month, day
}

func Index_Strings(data []string, dest string) int {
	for i, d := range data {
		if d == dest {
			return i
		}
	}
	return -1
}

func Index_Ints(data []int, dest int) int {
	for i, d := range data {
		if d == dest {
			return i
		}
	}
	return -1
}

func Index_Floats(data []float64, dest float64) int {
	for i, d := range data {
		if d == dest {
			return i
		}
	}
	return -1
}
